import java.util.Arrays;

public class WordNet {

    private ST<String, Bag<Integer>> nameToId = new ST<String, Bag<Integer>>();
    private ST<Integer, String[]> idToName = new ST<Integer, String[]>();
    private Digraph G;
    private SAP sap;

    // constructor takes the name of the two input files
    public WordNet(String synsets, String hypernyms) {
        In in = new In(synsets);
        int ids = 0;
        while (in.hasNextLine()) {
            String[] arr = in.readLine().split(",");
            int id = Integer.parseInt(arr[0]);
            String[] synset = arr[1].split(" ");
            for(String noun : synset) {
                Bag<Integer> prevIds = nameToId.get(noun);
                if(prevIds == null) {
                    prevIds = new Bag<Integer>();
                    nameToId.put(noun, prevIds);
                }
                prevIds.add(id);
            }
            idToName.put(id, synset);
            ids ++;
        }

        G = new Digraph(ids);
        in = new In(hypernyms);
        while (in.hasNextLine()) {
            String[] a = in.readLine().split(",");
            int id = Integer.parseInt(a[0]);
            for (int i = 1; i < a.length; i++) {
                int id2 = Integer.parseInt(a[i]);
                G.addEdge(id, id2);
            }
        }

        if (new DirectedCycle(G).hasCycle())
            throw new IllegalArgumentException();

        int roots = 0;
        for (int i = 0; i < G.V(); i++)
            if (!G.adj(i).iterator().hasNext()) {
                if (roots != 0)
                    throw new IllegalArgumentException();
                roots++;
            }

        sap = new SAP(G);
    }

    // returns all WordNet nouns
    public Iterable<String> nouns() {
        return nameToId.keys();
    }

    // is the word a WordNet noun?
    public boolean isNoun(String word) {
        return nameToId.contains(word);
    }

    // distance between nounA and nounB (defined below)
    public int distance(String nounA, String nounB) {
        if (!isNoun(nounA) || !isNoun(nounB))
            throw new IllegalArgumentException();

        return sap.length(nameToId.get(nounA), nameToId.get(nounB));
    }

    // a synset (second field of synsets.txt) that is the common ancestor of nounA and nounB
    // in a shortest ancestral path (defined below)
    public String sap(String nounA, String nounB) {
        if (!isNoun(nounA) || !isNoun(nounB))
            throw new IllegalArgumentException();

        int idx = sap.ancestor(nameToId.get(nounA), nameToId.get(nounB));
        String[] nouns = idToName.get(idx);
        String result = "";
        for(String n : nouns) {
            if (result.isEmpty())
                result += n;
            else
                result += " " + n;
        }
        return result;
    }

    // do unit testing of this class
    public static void main(String[] args) {
        WordNet w = new WordNet("synsets100-subgraph.txt", "hypernyms100-subgraph.txt");
        System.out.println(w.sap("whopper", "thing")); // thing' = 1
    }
}