import java.util.Arrays;
import java.util.Random;

public class SAP {

    private final Digraph G;

    // constructor takes a digraph (not necessarily a DAG)
    public SAP(Digraph G) {
        this.G = new Digraph(G);
    }

    // length of shortest ancestral path between v and w; -1 if no such path
    public int length(int v, int w) {
        return sap(v, w)[0];
    }

    // a common ancestor of v and w that participates in a shortest ancestral path; -1 if no such path
    public int ancestor(int v, int w) {
        return sap(v, w)[1];
    }

    // length of shortest ancestral path between any vertex in v and any vertex in w; -1 if no such path
    public int length(Iterable<Integer> v, Iterable<Integer> w) {
        if(v == null || w == null)
            throw new NullPointerException();

        return sap(v, w)[0];
    }

    // a common ancestor that participates in shortest ancestral path; -1 if no such path
    public int ancestor(Iterable<Integer> v, Iterable<Integer> w) {
        if(v == null || w == null)
            throw new NullPointerException();

        return sap(v, w)[1];
    }

    private int[] sap(Iterable<Integer> v,Iterable<Integer> w) {
        boolean[] markedV = new boolean[G.V()];
        boolean[] markedW = new boolean[G.V()];

        SET<Integer> setV = new SET<Integer>();

        Queue<Integer> q = new Queue<Integer>();
        for (int vv : v) {
            markedV[vv] = true;
            q.enqueue(vv);
            setV.add(vv);
        }
        for (int ww : w) {
            markedW[ww] = true;
            q.enqueue(ww);
            if (setV.contains(ww)) {
                return new int[]{0, ww};
            }
        }

        return process(q, markedV, markedW);
    }

    private int[] sap(int v, int w) {
        if (v == w) return new int[]{0, v};

        boolean[] markedV = new boolean[G.V()];
        boolean[] markedW = new boolean[G.V()];

        Queue<Integer> q = new Queue<Integer>();
        markedV[v] = true;
        q.enqueue(v);
        markedW[w] = true;
        q.enqueue(w);

        return process(q, markedV, markedW);
    }

    private int[] process(Queue<Integer> q, boolean[] markedV, boolean[] markedW) {

        int len = -1;
        int anc = -1;
        int[] distToV = new int[G.V()];
        int[] distToW = new int[G.V()];

        while (!q.isEmpty()) {
            int cur = q.dequeue();
            boolean markedVcur = markedV[cur];
            boolean markedWcur = markedW[cur];
            for (int x : G.adj(cur)) {
                boolean markedVx = markedV[x];
                boolean markedWx = markedW[x];

                if (markedVcur && !markedV[x]) {
                    markedV[x] = true;
                    distToV[x] = distToV[cur] + 1;
                }

                if (markedWcur && !markedW[x]) {
                    markedW[x] = true;
                    distToW[x] = distToW[cur] + 1;
                }

                if (markedV[x] && markedW[x]) {
                    int curLen = distToV[x] + distToW[x];
                    if (len == -1 || curLen < len) {
                        len = curLen;
                        anc = x;
                    }
                }

                if((markedVcur && !markedVx) || (markedWcur && !markedWx)) {
                    q.enqueue(x);
                }
            }
        }

        return new int[]{len, anc};
    }

    // do unit testing of this class
    public static void main(String[] args) {
        Digraph d = new Digraph(new In("digraph-wordnet.txt"));
        SAP s = new SAP(d);
        Bag<Integer> b1 = new Bag<Integer>();
        Bag<Integer> b2 = new Bag<Integer>();
        Random r = new Random();
        for(int i = 0; i < 5; i++){
            b1.add(r.nextInt(d.V()));
            b2.add(r.nextInt(d.V()));
        }
        long ms = System.currentTimeMillis();
        s.length(b1, b2);
        System.out.println("Time " + (System.currentTimeMillis() - ms));
    }
}