public class Outcast {
    private final WordNet wordnet;

    public Outcast(WordNet wordnet) {
        this.wordnet = wordnet;
    }

    public String outcast(String[] nouns) {
        int maxDist = 0;
        String outcast = null;

        for(String noun : nouns) {
            int dist = 0;
            for(String noun2 : nouns)
                if (!noun2.equals(noun)) {
                    dist += wordnet.distance(noun, noun2);
                }
            if (dist > maxDist) {
                maxDist = dist;
                outcast = noun;
            }
        }

        return outcast;
    }

    public static void main(String[] args) {
        // see test client below
    }
}